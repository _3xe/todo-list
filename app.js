//S
const todoInput = document.querySelector('.todo-input');
const todoButton = document.querySelector('.todo-button');
const todoList = document.querySelector('.todo-list');
const filter = document.querySelector('.filter-todo');

//EL
todoButton.addEventListener('click', addTodo);
todoList.addEventListener('click', deleteAndCheck);
filter.addEventListener('click', filterTodo);
document.addEventListener('DOMContentLoaded', getFromLocalStorage);

//F
function addTodo(e) {
  e.preventDefault();
  if(todoInput.value.length < 2) 
  {return;} else {
    const todoDiv = document.createElement('div');
    todoDiv.classList.add('todo');
    //
    const newTodo = document.createElement('li')
    newTodo.innerHTML=todoInput.value;
    newTodo.classList.add('todo-item');
    todoDiv.appendChild(newTodo);
    //
    saveToLocalStorage(todoInput.value)
    //
    const btnContainer = document.createElement('div');
    btnContainer.classList.add('btn-container');
    todoDiv.appendChild(btnContainer)
    //
    const completedButton = document.createElement('button');
    completedButton.innerHTML = '<i class="fas fa-check"></i>';
    completedButton.classList.add('complete-btn');
    btnContainer.appendChild(completedButton);
    //
    const trashButton = document.createElement('button');
    trashButton.innerHTML = '<i class="fas fa-trash"></i>';
    trashButton.classList.add('trash-btn');
    btnContainer.appendChild(trashButton);
    //
    todoList.appendChild(todoDiv);
    todoInput.value = "";
  }
}

function deleteAndCheck(e) {
  const item = e.target;
  if (item.classList[0] === 'trash-btn')
  {
    const todo = item.parentElement.parentElement;
    deleteFromLocalStorage(todo);
    todo.remove();
  }
  if (item.classList[0] === 'complete-btn')
  {
    const todo = item.parentElement.parentElement;
    todo.classList.toggle('completed');
  } 
}

function filterTodo() {
  const todos = todoList.children;
  console.log(todos);
  for(let i = 0; i < todos.length; i++) {
    switch(filter.value) {
      case 'all':
        todos[i].style.display = 'flex';
        console.log(filter.value)
      break;
      case 'completed':
        console.log(filter.value)
        if (todos[i].classList.contains('completed'))
        {
          todos[i].style.display = 'flex';
        } 
        else 
        {
          todos[i].style.display = 'none';
        }
      break;
      case 'uncompleted':
        console.log(filter.value)
        if (!todos[i].classList.contains('completed')) 
        {
          todos[i].style.display = 'flex';
        } 
        else 
        {
          todos[i].style.display = 'none';
        }
        break;
    }
  } 
}

//Local Storage

function saveToLocalStorage (todo) {
  let todos;
  if(localStorage.getItem('todos') === null) 
  {
    todos = [];
  } else {
    todos = JSON.parse(localStorage.getItem('todos'));
  }

  todos.push(todo);
  localStorage.setItem('todos', JSON.stringify(todos))
}

function getFromLocalStorage() {
  let todos;
  if(localStorage.getItem('todos') === null) 
  {
    todos = [];
  } else {
    todos = JSON.parse(localStorage.getItem('todos'));
  }

  todos.forEach(todo => {
    const todoDiv = document.createElement('div');
    todoDiv.classList.add('todo');
    //
    const newTodo = document.createElement('li')
    newTodo.innerHTML = todo;
    newTodo.classList.add('todo-item');
    todoDiv.appendChild(newTodo);
    //
    const btnContainer = document.createElement('div');
    btnContainer.classList.add('btn-container');
    todoDiv.appendChild(btnContainer)
    //
    const completedButton = document.createElement('button');
    completedButton.innerHTML = '<i class="fas fa-check"></i>';
    completedButton.classList.add('complete-btn');
    btnContainer.appendChild(completedButton);
    //
    const trashButton = document.createElement('button');
    trashButton.innerHTML = '<i class="fas fa-trash"></i>';
    trashButton.classList.add('trash-btn');
    btnContainer.appendChild(trashButton);
    //
    todoList.appendChild(todoDiv);
  });
}

function deleteFromLocalStorage (todo) {
  let todos;
  if(localStorage.getItem('todos') === null) 
  {
    todos = [];
  } else {
    todos = JSON.parse(localStorage.getItem('todos'));
  }

 const todoIndex = todo.children[0].innerText;
 todos.splice(todos.indexOf(todoIndex), 1);

 localStorage.setItem('todos', JSON.stringify(todos));
}